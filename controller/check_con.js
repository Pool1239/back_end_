var db = require('../connection/dbConnection')
var moment = require('moment-timezone')

exports.checkin = function () {
    return function (req, res, next) {
        db.query('SELECT timediff(date_out,date_in) times,timediff(time_out_break,time_in_break) break,timediff(timediff(date_out,date_in),timediff(time_out_break,time_in_break)) full_time\
          FROM checkin\
        ', function (err, result) {
            console.log(result)
            req.result = result;
            next();
        })
    }
}
exports.show_project = function () {
    return function (req, res, next) {
        db.query('select * from project p join company c on p.company_id=c.comID join employee e on p.user_create=e.empID order by project_id asc\
        ', function (err, result) {
            console.log(result)
            req.result = result;
            next();
        })
    }
}
exports.show_project_by_company = function () {
    return function (req, res, next) {
        db.query('select * from project p join company c on p.company_id=c.comID join employee e on p.user_create=e.empID\
         where company_id =\'' + req.body.company + '\'', function (err, result) {
            console.log(result)
            req.result = result;
            next();
        })
    }
}
exports.show_project2 = function () {
    return function (req, res, next) {
        db.query('SELECT * FROM project where project_id =\'' + req.body.project_id + '\'', function (err, result) {
            console.log(result)
            req.result = result;
            next();
        })
    }
}
exports.show_project3 = function () {
    return function (req, res, next) {
        db.query('SELECT * FROM project where project_id =\'' + req.body.project_id + '\'', function (err, result) {
            console.log(result)
            req.result = result;
            next();
        })
    }
}
exports.update_project = function () {
    return function (req, res, next) {
        db.query('UPDATE project SET project_name =\'' + req.body.project_name + '\',project_start =\'' + req.body.project_start + '\',project_end =\'' + req.body.project_end + '\'\
        ,user_create =\'' + req.body.user_create + '\' WHERE project_id =\'' + req.body.id + '\'', function (err, result) {
            req.result = result;
            next();  
        })
    }
}
exports.show_job = function () {
    return function (req, res, next) {
        console.log(req.body)
        db.query('SELECT * FROM job j join project p on j.project_id=p.project_id\
        join employee e on j.user_id=e.empID\
        where j.project_id =\'' + req.body.id + '\' order by date desc', function (err, result) {
            req.result = result.map(el => ({...el,full_name: el.emp_name + ' ' + el.emp_last }));
            next();  
        })
    }
}
exports.show_job_2 = function () {
    return function (req, res, next) {
        console.log(req.body)
        db.query('select j.user_id,emp_name,emp_last,j.project_id,project_name,date,hour,percent from job j join project p on j.project_id=p.project_id join employee e on j.user_id=e.empID\
        where user_id =\'' + req.body.user_id + '\' and date =\'' + req.body.date + '\' order by date desc', function (err, result) {
            req.result = result.map(el => ({...el,full_name: el.emp_name + ' ' + el.emp_last }));
            next();  
        })
    }
}
exports.show_checkin = function () {
    return function (req, res, next) {
        console.log(req.body)
        db.query('SELECT checkin_id,empID,emp_name,emp_last,date,date_in,date_out,time_in_break,time_out_break,comname,\
        timediff(date_out,date_in) times,timediff(time_out_break,time_in_break) breaks,\
        timediff(timediff(date_out,date_in),timediff(time_out_break,time_in_break)) full_time \
        FROM checkin c join employee e on c.user_id=e.empID\
        join company cpn on c.id_company=cpn.comID order by date desc', function (err, result) {
            req.result = result.map(el => ({...el,full_name: el.emp_name + ' ' + el.emp_last,full_times: full_daycon(el.times, el.breaks, el.full_time) }));
            next();  
        })
    }
}
exports.show_checkin_by_company = function () {
    return function (req, res, next) {
        var registerInfo = {
            date : moment.tz('Asia/Bangkok').format("YYYY-MM-DD"),
        }
        console.log(req.body)
        db.query('SELECT format((timediff(date_out,date_in))/10000,2) times,format((timediff(time_out_break,time_in_break))/10000,2) breaks,\
        format((timediff(date_out,date_in) - timediff(time_out_break,time_in_break))/10000,2) as full_time,\
        format(((timediff(date_out,date_in) - timediff(time_out_break,time_in_break))%10000)/10000,2) as full_time_2,\
        format(timediff(date_out,date_in)/100,2) full_time_3\
        FROM checkin where user_id =\'' + req.body.user_id + '\' and date =\'' + registerInfo.date + '\'', function (err, result) {
            req.result = result.map(el => ({...el,full_time: full_daycon2(el.times, el.breaks, el.full_time),full_time_2: full_daycon3(el.full_time_3, el.breaks,el.full_time_2) }));
            next();  
        })
    }
}
const full_daycon2 = (times, breaks, full_time) => {
    if (full_time == null) {
        return  times
    }
    else {
        return full_time
    }
}
const full_daycon3 = (full_time_3, breaks,full_time_2) => {
    if (full_time_2 == null) {
        return  (full_time_3 % 100)/100
    }
    else {
        return full_time_2
    }
}
exports.show_checkin_by_company2 = function () {
    return function (req, res, next) {
        var registerInfo = {
            date : moment.tz('Asia/Bangkok').format("YYYY-MM-DD"),
        }
        console.log(req.body)
        db.query('SELECT timediff(date_out,date_in) times,timediff(time_out_break,time_in_break) breaks,\
        timediff(timediff(date_out,date_in),timediff(time_out_break,time_in_break)) full_time \
        FROM checkin where user_id =\'' + req.body.user_id + '\' and date =\'' + registerInfo.date + '\'', function (err, result) {
            req.result = result.map(el => ({...el,full_time: full_daycon2(el.times, el.breaks, el.full_time) }));
            next();  
        })
    }
}
exports.show_checkin_3 = function () {
    return function (req, res, next) {
        var registerInfo = {
            date : moment.tz('Asia/Bangkok').format("YYYY-MM-DD"),
        }
        console.log(req.body)
        db.query('SELECT * FROM checkin \
        WHERE user_id =\'' + req.body.user_id + '\' and date =\'' + registerInfo.date + '\' and id_company =\'' + req.body.id_company + '\' ', function (err, result) {
            req.result = result;
            next();  
        })
    }
}

exports.show_checkin_2 = function () {
    return function (req, res, next) {
        console.log(req.body)
        db.query('SELECT empID,emp_name,emp_last,date\
        FROM checkin c join employee e on c.user_id=e.empID\
        where e.empID =\'' + req.body.empID + '\'', function (err, result) {
            req.result = result.map(el => ({...el,full_name: el.emp_name + ' ' + el.emp_last }));
            next();  
        })
    }
}
const full_daycon = (times, breaks, full_time) => {
    if (full_time == null) {
        return times
    }
    else {
        return full_time
    }
}
exports.insert_project = function () {
    return function (req, res, next) {
        var registerInfo = {
            project_name: req.body.project_name,
            company_id: req.body.company_id,
            user_create: req.body.user_create,
        }

        console.log(req.body)
        db.query("insert into project (project_name,company_id,user_create) VALUES\
        ('"+ registerInfo.project_name + "','"+ registerInfo.company_id + "','"+ registerInfo.user_create + "')", function (err, result) {
                if (err) throw err;
                req.result = result;
                next()
            })
    }
}
exports.insert_job = function () {
    return function (req, res, next) {
        var registerInfo = {
            project_id : req.body.project_id,
            user_id: req.body.user_id,
            hour: req.body.hour,
            percent: req.body.percent,
            date : moment.tz('Asia/Bangkok').format("YYYY-MM-DD") ,
        }

        console.log(req.body)
        db.query("insert into job (date,project_id,user_id,hour,percent) VALUES\
        ('"+ registerInfo.date + "','"+ registerInfo.project_id + "','"+ registerInfo.user_id + "','"+ registerInfo.hour + "','"+ registerInfo.percent + "')", function (err, result) {
                if (err) throw err;
                req.result = result;
                next()
            })
    }
}
exports.insert_checkin = function () {
    return function (req, res, next) {
        var registerInfo = {
            id_company : req.body.id_company,
            user_id: req.body.user_id,
            date : moment.tz('Asia/Bangkok').format("YYYY-MM-DD"),
            date_in : moment.tz('Asia/Bangkok').format("HH:mm:ss"),
        }

        console.log(req.body)
        db.query("insert into checkin (date,date_in,user_id,id_company) VALUES\
        ('"+ registerInfo.date + "','"+ registerInfo.date_in + "','"+ registerInfo.user_id + "','"+ registerInfo.id_company + "')", function (err, result) {
                if (err) throw err;
                req.result = result;
                next()
            })
    }
}
exports.update_checkin = function () {
    return function (req, res, next) {
        var registerInfo = {
            date : moment.tz('Asia/Bangkok').format("YYYY-MM-DD"),
            time_in_break : moment.tz('Asia/Bangkok').format("HH:mm:ss"),
        }

        console.log(req.body)
        db.query('UPDATE checkin SET time_in_break =\'' + registerInfo.time_in_break + '\'\
         WHERE user_id =\'' + req.body.user_id + '\' and date =\'' + registerInfo.date + '\' and id_company =\'' + req.body.id_company + '\'', function (err, result) {
            req.result = result;
            next();  
        })
    }
}
exports.update_checkin2 = function () {
    return function (req, res, next) {
        var registerInfo = {
            date : moment.tz('Asia/Bangkok').format("YYYY-MM-DD"),
            time_out_break : moment.tz('Asia/Bangkok').format("HH:mm:ss"),
        }

        console.log(req.body)
        db.query('UPDATE checkin SET time_out_break =\'' + registerInfo.time_out_break + '\'\
         WHERE user_id =\'' + req.body.user_id + '\' and date =\'' + registerInfo.date + '\' and id_company =\'' + req.body.id_company + '\'', function (err, result) {
            req.result = result;
            next();  
        })
    }
}
exports.update_checkin3 = function () {
    return function (req, res, next) {
        var registerInfo = {
            date : moment.tz('Asia/Bangkok').format("YYYY-MM-DD"),
            date_out : moment.tz('Asia/Bangkok').format("HH:mm:ss"),
        }

        console.log(req.body)
        db.query('UPDATE checkin SET date_out =\'' + registerInfo.date_out + '\'\
         WHERE user_id =\'' + req.body.user_id + '\' and date =\'' + registerInfo.date + '\' and id_company =\'' + req.body.id_company + '\'', function (err, result) {
            req.result = result;
            next();  
        })
    }
}