var db = require('../connection/dbConnection')
var moment = require('moment-timezone')
var bcrypt = require('bcryptjs')

exports.insert_company = function () {
    return function (req, res, next) {
        var registerInfo = {
            comname: req.body.comname,
        }

        console.log(req.body)
        db.query("insert into company (comname) VALUES\
        ('"+ registerInfo.comname + "')", function (err, result) {
                if (err) throw err;
                req.result = result;
                next()
            })
    }
}
exports.insert_lea_type = function () {
    return function (req, res, next) {
        let sql = "insert into lea_type (leaName_type,leaName_thai,max_day,IDcompany) VALUES ?";
        console.log(req.body)
        let { leaName_type, leaName_thai, max_day, IDcompany } = req.body
        const newVal = leaName_type.map((el, i) => [el, leaName_thai[i], max_day[i], IDcompany[i]])
        console.log(newVal)
        db.query(sql, [newVal], function (err) {
            if (err) throw err;
            else {
                next()
            }
        })
    }
}
exports.show_company = function () {
    return function (req, res, next) {
        db.query('SELECT comID,comname,time_in,time_out,count(empID) as countcompany FROM company c left join  employee e on c.comID = e.company group by comID', function (err, result) {
            console.log(result)
            req.result = result;
            next();

        })
    }
}
exports.show_company2 = function () {
    return function (req, res, next) {
        db.query('SELECT * FROM company where comID =\'' + req.body.comID + '\'', function (err, result) {
            console.log(result)
            req.result = result;
            next();
        })
    }
}
exports.count_company = function () {
    return function (req, res, next) {
        db.query('SELECT comID,comname,count(empID) as countcompany FROM company c left join  employee e on c.comID = e.company group by comID', function (err, result) {
            console.log(result)
            req.result = result;
            next();
        })
    }
}