// var errorMessages = require('../const/error_messages')
var db = require('../connection/dbConnection')
var json2csv = require('json2csv').parse;
// var constance = require('../const/constance')
// var moment = require('moment-timezone')

exports.show_benefit = function () {
    return function (req, res, next) {
        db.query('SELECT *, e.emp_name,e.emp_last,ed.userName as nameuser,ed.empID as addmin FROM benefit b join ben_type bt on b.ben_type=bt.benID_type\
         join status s on b.status = s.staID join employee e on b.empID = e.empID join employee ed on b.add_empID = ed.empID join company c on e.company=c.comID\
         order by benID asc ', function (err, result) {
            console.log(result)
            req.result = result.map(el => ({ ...el, full_name: el.emp_name +' ' + el.emp_last}));
            next();
            
        })
    }
}
exports.show_benefit_by_company = function () {
    return function (req, res, next) {
        db.query('SELECT *, e.emp_name,e.emp_last,ed.userName as nameuser,ed.empID as addmin FROM benefit b join ben_type bt on b.ben_type=bt.benID_type join status s on b.status = s.staID join employee e on b.empID = e.empID join employee ed on b.add_empID = ed.empID where e.company =\'' + req.body.company + '\' order by benID asc ', function (err, result) {
            console.log(result)
            req.result = result.map(el => ({ ...el, full_name: el.emp_name +' ' + el.emp_last}));
            next();
            
        })
    }
}
exports.export_benefit = function () {
    return function (req, res, next) {
        db.query('SELECT *, e.emp_name,e.emp_last,ed.userName as nameuser,ed.empID as addmin FROM benefit b join ben_type bt on b.ben_type=bt.benID_type join status s on b.status = s.staID join employee e on b.empID = e.empID join employee ed on b.add_empID = ed.empID where ben_date_request between \'' + req.params.date1 + '\' and \'' + req.params.date2 + '\' order by benID asc ', function (err, result) {
            req.result = result.map(el => ({ benID:el.benID, full_name: el.emp_name +' ' + el.emp_last,benName_type:el.benName_type,ben_date_request:el.ben_date_request,ben_date_receipt:el.ben_date_receipt,amountl:el.amount,sta_name:el.sta_name,nameuser:el.nameuser,flexible_created:el.flexible_created}));
            var csv = json2csv(req.result)
            res.attachment('filename.csv');
            res.status(200).send(csv);
        })
    }
}

exports.export_benefit_by_company = function () {
    return function (req, res, next) {
        db.query('SELECT *, e.emp_name,e.emp_last,ed.userName as nameuser,ed.empID as addmin FROM benefit b join ben_type bt on b.ben_type=bt.benID_type join status s on b.status = s.staID\
         join employee e on b.empID = e.empID join employee ed on b.add_empID = ed.empID where ben_date_request between \'' + req.params.date1 + '\' and \'' + req.params.date2 + '\'\
         and e.company =\'' + req.params.company + '\' order by benID asc ', function (err, result) {
            req.result = result.map(el => ({ benID:el.benID, full_name: el.emp_name +' ' + el.emp_last,benName_type:el.benName_type,ben_date_request:el.ben_date_request,ben_date_receipt:el.ben_date_receipt,amountl:el.amount,sta_name:el.sta_name,nameuser:el.nameuser,flexible_created:el.flexible_created}));
            var csv = json2csv(req.result)
            res.attachment('filename.csv');
            res.status(200).send(csv);
        })
    }
}

exports.show_benefit_1 = function () {
    return function (req, res, next) {
        db.query('SELECT * FROM benefit b join ben_type bt on b.ben_type = bt.benID_type WHERE benID =\'' +  req.body.benID + '\'', function (err, result) {
            console.log(result)
            req.result = result;
            next();
            
        })
    }
}
exports.show_benefit_2 = function () {
    return function (req, res, next) {
        db.query('SELECT e.userName as userName FROM benefit b join employee e on b.add_empID = e.empID', function (err, result) {
            console.log(result)
            req.result = result;
            next();
            
        })
    }
}
exports.update_benefit = function () {
    return function (req, res, next) {
        db.query('UPDATE benefit SET status = 2,add_empID =\'' + req.body.add_empID + '\'  WHERE benID =\'' + req.body.benID + '\'', function (err, result) {
            req.result = result;
            next();  
        })
    }
}

exports.update_benefit_1 = function () {
    return function (req, res, next) {
        db.query('UPDATE benefit SET status = 3,add_empID =\'' + req.body.add_empID + '\' WHERE benID =\'' +  req.body.benID + '\'', function (err, result) {
            req.result = result;
            next();  
        })
    }
}