// var errorMessages = require('../const/error_messages')
var db = require('../connection/dbConnection')
// var constance = require('../const/constance')
// var moment = require('moment-timezone')

exports.get_leave_type = function () {
    return function (req, res, next) {
        db.query('SELECT *  FROM lea_type', function (err, result) {
            req.result = result;
            next();
            
        })
    }
}

exports.get_inner_join = function () {
    return function (req, res, next) {
        db.query('select * from employee e inner join leave_request l on e.empID = l.empID', function (err, result) {
            req.result = result;
            next();   
        })
    }
}

exports.get_leave_type_by_id = function () {
    return function (req, res, next) {
        db.query('SELECT *  FROM lea_type WHERE leaID_type =\'' +  req.body.id+ '\'', function (err, result) {
            req.result = result;
            next();
            
        })
    }
}

exports.get_leave_type_from_id = function () {
    return function (req, res, next) {
        db.query('SELECT *  FROM lea_type WHERE leaID_type between \'' +  req.params.id + '\' and \'' +  req.params.ib + '\'', function (err, result) {
            req.result = result;
            next();
            
        })
    }
}