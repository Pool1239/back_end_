// var errorMessages = require('../const/error_messages')
var db = require('../connection/dbConnection')
var json2csv = require('json2csv').parse;
// var constance = require('../const/constance')
// var moment = require('moment-timezone')

exports.show_leave_request = function () {
    return function (req, res, next) {
        db.query('SELECT c.comname,l.leaID as leaveID,e.empID as Empid,e.emp_name,e.emp_last,leaName_day,lea_day,leaName_type,date_start,date_end,staID,sta_name,ed.userName as nameuser,ed.empID as addmin\
        ,l.lea_reason,r.rol_name,l.date_created,DATEDIFF(date_end,date_start)+1 as datediff,TIMEDIFF(date_end,date_start) as timediff FROM leave_request l join employee e on l.empID = e.empID\
         join employee ed on l.add_empID = ed.empID join lea_day ld on l.lea_day=ld.leaID_day join lea_type lt on l.lea_type=lt.leaID_type join status s on l.status=s.staID\
          join role r on e.role=r.rolID join company c on e.company=c.comID order by e.empID asc ', function (err, result) {

            req.result = result.map(el => ({ ...el, full_name: el.emp_name + ' ' + el.emp_last, full_day: full_daycon(el.lea_day, el.datediff, el.timediff) }));
            next();

        })
    }
}
exports.show_leave_request_by_company = function () {
    return function (req, res, next) {
        db.query('SELECT l.leaID as leaveID,e.empID as Empid,e.emp_name,e.emp_last,leaName_day,lea_day,leaName_type,date_start,date_end,staID,sta_name,ed.userName as nameuser,ed.empID as addmin\
        ,l.lea_reason,r.rol_name,l.date_created,DATEDIFF(date_end,date_start)+1 as datediff,TIMEDIFF(date_end,date_start) as timediff FROM leave_request l join employee e on l.empID = e.empID\
         join employee ed on l.add_empID = ed.empID join lea_day ld on l.lea_day=ld.leaID_day join lea_type lt on l.lea_type=lt.leaID_type join status s on l.status=s.staID join role r\
          on e.role=r.rolID where e.company =\'' + req.body.company + '\' order by e.empID asc ', function (err, result) {
            req.result = result.map(el => ({ ...el, full_name: el.emp_name + ' ' + el.emp_last, full_day: full_daycon(el.lea_day, el.datediff, el.timediff) }));
            next();

        })
    }
}

exports.export_leave_request = function () {
    return function (req, res, next) {
        // var date1 = req.body.date1.split('""')[1];
        // var date2 = req.body.date2.split('""')[1];
        db.query('SELECT l.leaID as leaveID,e.empID as Empid,e.emp_name,e.emp_last,leaName_day,lea_day,leaName_type,date_start,date_end,staID,sta_name,ed.userName as nameuser,\
        ed.empID as addmin,l.lea_reason,r.rol_name,l.date_created,DATEDIFF(date_end,date_start)+1 as datediff,TIMEDIFF(date_end,date_start) as timediff FROM leave_request l \
        join employee e on l.empID = e.empID join employee ed on l.add_empID = ed.empID join lea_day ld on l.lea_day=ld.leaID_day join lea_type lt on l.lea_type=lt.leaID_type \
        join status s on l.status=s.staID join role r on e.role=r.rolID where date_start between \'' + req.params.date1 + '\' and \'' + req.params.date2 + '\'', function (err, result) {
            req.result = result.map(el => ({ full_name: el.emp_name + ' ' + el.emp_last, lea_type: el.leaName_type, lea_day: el.leaName_day, date_start: el.date_start, date_end: el.date_end, full_day: full_daycon(el.lea_day, el.datediff, el.timediff), status: el.sta_name, date_created: el.date_created }));
          
            var csv = json2csv(req.result)
            res.attachment('filename.csv');
            res.status(200).send(csv);

        })
    }
}

exports.export_leave_request_by_company = function () {
    return function (req, res, next) {
        // var date1 = req.body.date1.split('""')[1];
        // var date2 = req.body.date2.split('""')[1];
        db.query('SELECT l.leaID as leaveID,e.empID as Empid,e.emp_name,e.emp_last,leaName_day,lea_day,leaName_type,date_start,date_end,staID,sta_name,ed.userName as nameuser\
        ,ed.empID as addmin,l.lea_reason,r.rol_name,l.date_created,DATEDIFF(date_end,date_start)+1 as datediff,TIMEDIFF(date_end,date_start) as timediff FROM leave_request l \
        join employee e on l.empID = e.empID join employee ed on l.add_empID = ed.empID join lea_day ld on l.lea_day=ld.leaID_day join lea_type lt on l.lea_type=lt.leaID_type \
        join status s on l.status=s.staID join role r on e.role=r.rolID where date_start between \'' + req.params.date1 + '\' and \'' + req.params.date2 + '\' \
        and e.company =\'' + req.params.company + '\'', function (err, result) {
            req.result = result.map(el => ({ full_name: el.emp_name + ' ' + el.emp_last, lea_type: el.leaName_type, lea_day: el.leaName_day, date_start: el.date_start, date_end: el.date_end, full_day: full_daycon(el.lea_day, el.datediff, el.timediff), status: el.sta_name, date_created: el.date_created }));
          
            var csv = json2csv(req.result)
            res.attachment('filename.csv');
            res.status(200).send(csv);

        })
    }
}

const full_daycon = (lea_day, datediff, timediff) => {
    if (lea_day == 1) {
        return datediff + ' day'
    }
    else {
        return timediff + ' hrs'
    }
}

