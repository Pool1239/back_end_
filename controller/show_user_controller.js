// var errorMessages = require('../const/error_messages')
var db = require('../connection/dbConnection')
// var constance = require('../const/constance')
// var moment = require('moment-timezone')

exports.show_user = function () {
    return function (req, res, next) {
        db.query('SELECT * FROM employee e join pro_period p on e.pro_period = p.proID join role r on e.role = r.rolid\
         join company c on e.company=c.comID order by empID asc;', function (err, result) {
            console.log(result)
            req.result = result.map(el => ({ ...el, full_name: el.emp_name + ' ' + el.emp_last, admin_email: result.find(e => e.empID === el.user_create_id).email }));
            next();

        })
    }
}
exports.show_user_by_company = function () {
    return function (req, res, next) {
        db.query('select e.empID,e.emp_name,e.emp_last,e.userName,e.email,e.starting_date,rol_name,pro_name,e.user_create_id,ed.email as admin_email\
            from employee e join role r on e.role=r.rolID join pro_period p on e.pro_period=p.proID join employee ed on e.user_create_id=ed.empID\
            where e.company =\'' + req.body.company + '\'', function (err, result) {
                console.log(result)
                req.result = result.map(el => ({ ...el, full_name: el.emp_name + ' ' + el.emp_last }));
                next();
            })
    }
}

exports.show_user2 = function () {
    return function (req, res, next) {
        db.query('SELECT count(empID) as sumid FROM employee;', function (err, result) {
            console.log(result)
            req.result = result;
            next();

        })
    }
}
exports.show_user2_by_company = function () {
    return function (req, res, next) {
        db.query('SELECT count(empID) as sumid FROM employee where company =\'' + req.body.company + '\'', function (err, result) {
            console.log(result)
            req.result = result;
            next();

        })
    }
}


 