var db = require('../connection/dbConnection')
var moment = require('moment-timezone')
var bcrypt = require('bcryptjs')

exports.insert_user = function () {
    return function (req, res, next) {
        var registerInfo = {
            emp_name: req.body.emp_name,
            emp_last: req.body.emp_last,
            userName: req.body.userName,
            role: req.body.role,
            email: req.body.email,
            pro_period: req.body.pro_period,
            starting_date: moment.tz('Asia/Bangkok').format("YYYY-MM-DD HH:mm:ss"),
            user_create_id: req.body.user_create_id,
        }

        console.log(req.body)
        db.query("INSERT INTO employee (emp_name,emp_last,userName,role,email,pro_period,starting_date,user_create_id) VALUES\
        ('"+ registerInfo.emp_name + "', '" + registerInfo.emp_last + "', '" + registerInfo.userName + "', '" + registerInfo.role
            + "', '" + registerInfo.email + "', '" + registerInfo.pro_period + "', '" + registerInfo.starting_date + "', '" + registerInfo.user_create_id + "')", function (err, result) {
                if (err) throw err;
                req.result = result;
                next()
            })
    }
}

exports.insert_user_by_company = function () {
    return function (req, res, next) {
        var registerInfo = {
            emp_name: req.body.emp_name,
            emp_last: req.body.emp_last,
            userName: req.body.userName,
            role: req.body.role,
            email: req.body.email,
            pro_period: req.body.pro_period,
            starting_date: moment.tz('Asia/Bangkok').format("YYYY-MM-DD HH:mm:ss"),
            user_create_id: req.body.user_create_id,
            company: req.body.company,
        }

        console.log(req.body)
        db.query("INSERT INTO employee (emp_name,emp_last,userName,role,email,pro_period,starting_date,user_create_id,company) VALUES\
        ('"+ registerInfo.emp_name + "', '" + registerInfo.emp_last + "', '" + registerInfo.userName + "', '" + registerInfo.role
            + "', '" + registerInfo.email + "', '" + registerInfo.pro_period + "', '" + registerInfo.starting_date + "', '" + registerInfo.user_create_id 
            + "', '" + registerInfo.company + "')", function (err, result) {
                if (err) throw err;
                req.result = result;
                next()
            })
    }
}

exports.change_password = function () {
    return function (req, res, next) {

        let password = bcrypt.hashSync(req.body.password)
        db.query(`SELECT empID,email FROM employee WHERE email = '${req.body.email}'`, function (err, result) {
            if (err) throw err;
            if (result.length > 0) {
                db.query(`UPDATE employee SET password = '${password}'  WHERE empID = '${result[0].empID}'`, function (err, result) {
                    if (err) throw err;
                    req.result = result;
                    next()
                })
            }
            else{
                res.status(201).json({success : false , message :"Email ไม่ถูกต้อง"})
            }
        })

    }
}


