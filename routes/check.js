const express = require('express')
const router = express.Router()
const userUtil = require('../controller/check_con')
// const customerUtil = require('../controllers/customer_controller')
const validateUtil = require('../controller/validate_controller')
var path = require('path')

router.get('/checkin',
    validateUtil.validate_token(),
    validateUtil.validate_admin(),
    // customerUtil.create_customer(),
    userUtil.checkin(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
})
router.get('/project',
    validateUtil.validate_token(),
    validateUtil.validate_admin(),
    // customerUtil.create_customer(),
    userUtil.show_project(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
})
router.post('/show_project_by_company',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.show_project_by_company(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
})
router.post('/insert_project',
    validateUtil.validate_token(),
    validateUtil.validate_admin(),
    // customerUtil.create_customer(),
    userUtil.insert_project(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
})
router.post('/insert_job',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.insert_job(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
})
router.post('/insert_checkin',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.insert_checkin(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
})
router.post('/update_checkin',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.update_checkin(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
})
router.post('/update_checkin2',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.update_checkin2(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
})
router.post('/update_checkin3',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.update_checkin3(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
})
router.post('/show_project2',
    validateUtil.validate_token(),
    validateUtil.validate_admin(),
    // customerUtil.create_customer(),
    userUtil.show_project2(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
})
router.post('/show_project3',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.show_project3(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
})
router.post('/update_project',
    validateUtil.validate_token(),
    validateUtil.validate_admin(),
    // customerUtil.create_customer(),
    userUtil.update_project(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
})
router.post('/show_job',
    validateUtil.validate_token(),
    validateUtil.validate_admin(),
    // customerUtil.create_customer(),
    userUtil.show_job(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
})
router.post('/show_job_2',
    validateUtil.validate_token(),
    validateUtil.validate_admin(),
    // customerUtil.create_customer(),
    userUtil.show_job_2(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
})
router.get('/show_checkin',
    validateUtil.validate_token(),
    validateUtil.validate_admin(),
    // customerUtil.create_customer(),
    userUtil.show_checkin(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
})
router.post('/show_checkin_by_company',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.show_checkin_by_company(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
})
router.post('/show_checkin_by_company2',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.show_checkin_by_company2(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
})
router.post('/show_checkin_2',
    validateUtil.validate_token(),
    validateUtil.validate_admin(),
    // customerUtil.create_customer(),
    userUtil.show_checkin_2(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
})
router.post('/show_checkin_3',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.show_checkin_3(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
})
module.exports = router