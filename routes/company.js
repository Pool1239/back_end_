const express = require('express')
const router = express.Router()
const userUtil = require('../controller/company_controller')
// const customerUtil = require('../controllers/customer_controller')
const validateUtil = require('../controller/validate_controller')
var path = require('path')

router.post('/insert_company',
    validateUtil.validate_token(),
    validateUtil.validate_admin(),
    // customerUtil.create_customer(),
    userUtil.insert_company(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
})

router.post('/insert_lea_type',
    validateUtil.validate_token(),
    validateUtil.validate_admin(),
    // customerUtil.create_customer(),
    userUtil.insert_lea_type(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
})

router.get('/show_company',
    validateUtil.validate_token(),
    validateUtil.validate_admin(),
    // customerUtil.create_customer(),
    userUtil.show_company(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })
    router.post('/show_company2',
    validateUtil.validate_token(),
    validateUtil.validate_admin(),
    // customerUtil.create_customer(),
    userUtil.show_company2(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })
    router.get('/count_company',
    validateUtil.validate_token(),
    validateUtil.validate_admin(),
    // customerUtil.create_customer(),
    userUtil.count_company(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })

module.exports = router