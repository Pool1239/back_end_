const express = require('express')
const router = express.Router()
const userUtil = require('../controller/show_my_leave_request')
// const customerUtil = require('../controllers/customer_controller')
const validateUtil = require('../controller/validate_controller')


router.post('/show_my_leave_request_1',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.show_my_leave_request_1(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })
    router.post('/show_my_leave_request_1_1',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.show_my_leave_request_1_1(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })
    router.post('/show_my_leave_request_2',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.show_my_leave_request_2(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })
    router.post('/show_my_leave_request_2_1',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.show_my_leave_request_2_1(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })
    router.post('/show_my_leave_request_3',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.show_my_leave_request_3(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })
    router.post('/show_my_leave_request_3_1',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.show_my_leave_request_3_1(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })
    router.post('/show_my_leave_request_4',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.show_my_leave_request_4(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })
    router.post('/show_my_leave_request_4_1',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.show_my_leave_request_4_1(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })
    router.post('/show_my_leave_request_5',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.show_my_leave_request_5(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })
    router.post('/show_my_leave_request_5_1',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.show_my_leave_request_5_1(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })
    router.post('/show_my_leave_request_6',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.show_my_leave_request_6(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })
    router.post('/show_my_leave_request_6_1',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.show_my_leave_request_6_1(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })
    router.post('/show_my_leave_request_7',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.show_my_leave_request_7(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })
    router.post('/show_my_leave_request_7_1',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.show_my_leave_request_7_1(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })
    router.post('/show_my_leave_request_8',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.show_my_leave_request_8(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })

    router.post('/show_my_leave_request_9',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.show_my_leave_request_9(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })
    
    router.post('/show_max_day',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.show_max_day(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })
    router.post('/show_max_day2',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.show_max_day2(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })
    router.post('/update_leave_type',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.update_leave_type(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })
    router.post('/update_company',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.update_company(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })

    router.post('/show_request_count_company',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.show_request_count_company(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })
module.exports = router