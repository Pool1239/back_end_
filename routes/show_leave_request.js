const express = require('express')
const router = express.Router()
const userUtil = require('../controller/show_leave_request_controller')
// const customerUtil = require('../controllers/customer_controller')
const validateUtil = require('../controller/validate_controller')

router.get('/show_leave_request',
    validateUtil.validate_token(),
    validateUtil.validate_admin(),
    // customerUtil.create_customer(),
    userUtil.show_leave_request(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })
router.post('/show_leave_request_by_company',
    validateUtil.validate_token(),
    validateUtil.validate_admin(),
    // customerUtil.create_customer(),
    userUtil.show_leave_request_by_company(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })

router.get('/export_leave_request/:date1/:date2',
    validateUtil.validate_token(),
    validateUtil.validate_admin(),
    // customerUtil.create_customer(),
    userUtil.export_leave_request(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })

router.get('/export_leave_request_by_company/:date1/:date2/:company',
    validateUtil.validate_token(),
    validateUtil.validate_admin(),
    // customerUtil.create_customer(),
    userUtil.export_leave_request_by_company(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })


module.exports = router