const express = require('express')
const router = express.Router()
const userUtil = require('../controller/show_user_controller')
// const customerUtil = require('../controllers/customer_controller')
const validateUtil = require('../controller/validate_controller')

router.get('/show_user',
    validateUtil.validate_token(),
    validateUtil.validate_admin(),
    // customerUtil.create_customer(),
    userUtil.show_user(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })

router.post('/show_user_by_company',
    validateUtil.validate_token(),
    validateUtil.validate_admin(),
    // customerUtil.create_customer(),
    userUtil.show_user_by_company(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })

router.get('/show_user2',
    validateUtil.validate_token(),
    validateUtil.validate_admin(),
    // customerUtil.create_customer(),
    userUtil.show_user2(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })

router.post('/show_user2_by_company',
    validateUtil.validate_token(),
    validateUtil.validate_admin(),
    // customerUtil.create_customer(),
    userUtil.show_user2_by_company(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })

module.exports = router