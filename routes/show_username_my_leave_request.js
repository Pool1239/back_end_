const express = require('express')
const router = express.Router()
const userUtil = require('../controller/show_username_my_leave_request_con')
// const customerUtil = require('../controllers/customer_controller')
const validateUtil = require('../controller/validate_controller')

router.post('/show_username_my_leave_request',
    validateUtil.validate_token(),
    // customerUtil.create_customer(),
    userUtil.show_username_my_leave_request(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })

module.exports = router